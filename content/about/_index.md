+++
title = "About"
description = "Eclectic rebel, nighthawk, & eco-geek"
+++

<p>&nbsp;</p>

<img style="float:right" src="/images/mjbrown.png" alt="Murray J Brown">

I am a recovering software executive, recently semi-retired, now pursuing part-time freelance work as a copy editor and technical writer.

As an advocate for environmental and social justice I’m interested in issues of ecological sustainability, climate change, peak resources, biodiversity, food & water security, global development, and a post-growth economy.

With an off-beat, dark, wickedly irreverent and sarcastic sense of humour, and an especial fondness for puns, I have an absurdist worldview, with deep respect for Advaita, Daoist, Stoic, and Zen philosophies. Politically, I identify as an ecosocialist, a green democratic socialist with left-libertarian tendencies, without any party affiliation.

Over my career in software product development, I have experience with a variety of web technologies, distributed systems, real-time and embedded systems, electronic commerce, secure messaging, public key infrastructure (PKI), and information security. As a software engineering leader I’ve taken a pragmatic, hands-on, application-oriented, systems design approach to the architecture, design, and implementation of trustworthy web software infrastructure and applications. Most recently, I've focused on web development as a full-stack developer, with emphasis on backend applications and infrastructure.

I encourage the use of free/libre open source software (FLOSS). My open source software experience began with Unix V6, the original non-commercial distribution by AT&T Bell Labs to academic institutions (circa '78). I’m a long-time Linux user; Yggdrasil was my first distro (circa '93); Debian for past two decades.

I value integrity, honesty, intelligence, passion, compassion, fair play, tolerance, humour, adventure, an open mind,  life-long learning, a stout heart, and a sense of purpose.


### Contact

<a class="mailto" href="mailto:" target="_blank"><img src="/images/mailto.png" alt="Click to send email." title="Click to send email." /></a>


### Elsewhere

- <a target="_blank" rel="noopener noreferrer nofollow" href="https://www.linkedin.com/in/murrayjbrown">LinkedIn</a> career profile
- <a target="_blank" rel="noopener noreferrer" href="https://github.com/murrayjbrownn">Github</a> software portfolio
- <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/mjbrown">Gitlab</a> software projects
- <a target="_blank" rel="noopener noreferrer" href="https://www.bibsonomy.org/user/mjbrown">Bibsonomy</a> web bookmarks
- <a target="_blank" rel="noopener noreferrer" href="https://www.librarything.com/catalog/m.j.brown">LibraryThing</a> book catalogue
- <a target="_blank" rel="noopener noreferrer" href="https://pluspora.com/people/786e2560ad8f013646b9005056268def">Pluspora</a> Diaspora pod
- <a target="_blank" rel="noopener noreferrer" href="https://linktr.ee/m.j.brown">Linktr.ee</a> social links


### Colophon

This website was produced via the <a rel="noopener noreferrer" target="_blank" href="https://getzola.org">Zola</a> static site generator (written in Rust), using the <a rel="noopener noreferrer" target="_blank" href="https://spec.commonmark.org">CommonMark</a> dialect of Markdown, and is hosted on <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com">Gitlab</a>.
