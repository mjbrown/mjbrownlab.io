+++
title = "Services"
description = ""
+++

<br>&nbsp;</br>

I offer a range of services for writing, editing, and presenting information in a variety of formats, specializing in expository material for academic and technically savvy audiences. Moreover, I can assist in the research and synthesis of additional information for a topic, as needed. 

For more information or a quotation, please <a class="mailto" href="mailto: " target="_blank">email me</a>.


<table>
<thead>
<tr>  
 <th>Service</th>
 <th>Description</th>
</tr>

<tr>  
 <th>

&nbsp;

*Review/Revise*

 </th>
 <th></th>
</tr>
</thead>
</tbody>

<tbody>

<tr name="proofreading">
 <td>Proofreading</td>
 <td>
Review a final draft (i.e., print-ready) text for mistakes in capitalization, spelling, hyphenation, abbreviations, punctuation, and the way numbers are treated; it also includes attention to grammar, syntax, usage, and consistency of style. Feedback shall be provided by way of annotation, although simple mechanical editing may be performed for minor corrections.
 </td>
</tr>

<tr name="copyediting">
 <td>Copy editing</td>
 <td>
Mechanical editing entails finding and correcting mistakes in capitalization, spelling, hyphenation, abbreviations, punctuation, and the way numbers are treated; it also includes attention to grammar, syntax, and usage. Substantive editing deals with the organization and presentation of material. This may involve minor wordsmithing to improve style or to eliminate ambiguity, or localized reorganization (e.g., tightening) of the text.
 </td>
</tr>

<tr name="devediting">
 <td>Developmental editing</td>
 <td>
Reviewing an early draft to identify where material changes in content, organization, or style may improve the work. Substantive wordsmithing or modifying text to improve clarity and flow, without changing its intended meaning.
 </td>
</tr>

<tr name="conformance">
 <td>
Style conformance
 </td>
 <td>
Ensuring text conforms to the requirements of a recognized <a target="_blank" rel="noopener noreferrer nofollow" href="https://www.thebalancesmb.com/which-style-guide-should-i-use-1360722">writing style guide</a>: Associated Press (AP), American Psychological Association (APA), Chicago Manual of Style (CMS), Modern Language Association (MLA).
 </td>
</tr>

<tr name="factchecking">
 <td>Fact checking</td>
 <td>
Reviewing content for factual correctness and citation of reliable sources.
 </td>
</tr>

<tr>
 <td></td>
 <td></td>
</tr>
</tbody>

<thead>
<tr>
 <th>

*Writing*

 </th>
 <th></th>
</tr>
</thead>
</tbody>

<tbody>
<tr name="expositoryarticle">
 <td>Expository article</td>
 <td>
Authoring of an article that explores some topic, in simple language to make the concept clear for everyone.
 </td>
</tr>

<tr name="technicaldocument">
 <td>Technical document</td>
 <td>
Authoring of a document providing information about the underlying architecture, materials, and process for interfacing with, or building on top of, existing technology. (End-user documentation excluded.)
 </td>
</tr>


<tr name="research">
 <td>Research</td>
 <td>
Identifying additional sources for topical material that might be included or referenced within a written work.
 </td>
</tr>

<tr>
 <td></td>
 <td></td>
</tr>
</tbody>


<thead>
<tr>
 <th>

*Production*

 </th>
 <th></th>
</tr>
</thead>

<tbody>
<tr name="documentdesign">
 <td>
Design
</td>
 <td>
Layout and styling of document elements (e.g., titles, headings, paragraphs, citations, figures, tables, headers, footers, etc.)
</td>
</tr>

<tr name="documentconversion">
 <td>Conversion</td>
 <td>Convert document from one of specified file format to another (e.g., .DOCX, .EPUB, .HTML, .ODT, .PDF, etc.).</td>
</tr>

<tr>
 <td></td>
 <td></td>
</tr>
</tbody>

</table>

