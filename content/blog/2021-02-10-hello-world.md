+++
title = "Welcome"
description = "Launch of my new website"

[extra]
comments = false
+++

Having recently retired from my career as a software developer and now seeking opportunities for part-time remote work as a technical writer and web developer, I figured I needed a new website to reflect this change in orientation.

This is a statically generated site produced via <a target="_blank" rel="noopener noreferrer" href="https://getzola.org">Zola</a>, which is written in Rust and very fast. It is hosted at <a target="_blank" rel="noopener noreferrer" href="https://gitlab.io">Gitlab</a>, where its version controlled source repository is maintained and used for content management. I use the  <a target="_blank" rel="noopener noreferrer" href="https://joker.com">Joker</a>'s domain name services to map ***mjbrown.com*** onto ***mjbrown.gitlab.io***.