+++
title = ""
description = ""
+++

I work as a freelance copy editor and technical writer, part-time since retiring from my career in software development.

<!-- ### Writing and editing -->

The ability to write well and clearly explain complex concepts has always been important to me for communicating ideas, strategies, plans, and technical aspects of projects. I offer a range of services for copy editing and expository writing (e.g., academic papers, technical documents). These include editing for grammar, punctuation, spelling, wordsmithing, and styling a document.

Additionally, I can assist with research, fact checking, or make suggestions to improve some written work. Whether you have a conceptual draft that needs development or a final manuscript that just needs checking or light editing, I will work with you to refine it as needed for your target audience.

&nbsp;

<table style="width: 100%; border: 1px solid black;text-align: center;">
<tbody>
<tr>
<td style="width: 25%; border: 1px solid black;text-align: center;"><a href="/services#developmental">proofreading</a></td>
<td style="width: 25%; border: 1px solid black;text-align: center;"><a href="/services#copyediting">copy editing</a></td>
</tr>
<tr>
<td style="width: 25%; border: 1px solid black;text-align: center;"><a href="/services#conformance">style conformance</a></td>
<td style="width: 25%; border: 1px solid black;text-align: center;"><a href="/services#factchecking">fact checking</a></td>
</tr>
<tr>
<td style="width: 25%; border: 1px solid black;text-align: center;"><a href="/services#expositorywriting">expository articles</a></td>
<td style="width: 25%; border: 1px solid black;text-align: center;"><a href="/services#technicaldocument">technical writing</a></td>
</tr>
<tr>
<td style="width: 25%; border: 1px solid black;text-align: center;"><a href="/services#documentdesign">layout & design</a></td>
<td style="width: 25%; border: 1px solid black;text-align: center;"><a href="/services#research">research</a></td>
</tr>
</tbody>
</table>

<!--
### Web publishing

I offer consulting services for developing simple <a href="/services#webdev">static websites</a> for individuals and organizations that have relatively basic requirements for publishing on the web.

Whilst historically there has been a trend towards ever more complex dynamic websites, there has been a resurgence of much simpler solutions for those with more basic requirements. Using a site generator program, such as  <a target="_blank" rel="noopener noreferrer nofollow" href="https://gohugo.io">Hugo</a> or <a target="_blank" rel="noopener noreferrer nofollow" href="https://getzola.org">Zola</a>, site content that can be easily prepared or updated offline, rendered into web pages, previewed, and then uploaded to be published.

After many years as a full-stack web developer, I've come to embrace the simplicity of the static generator approach:

- Website content is authored using your favourite text editor and stored in easily handled plain text files, not in a database from which it may be difficult to export, migrate, or repurpose.
- The pages are typically written in <a target="_blank" rel="noopener noreferrer nofollow" href="https://www.markdownguide.org/">Markdown</a>, a simple markup language that is easier to learn and use than HTML, which is the lingua franca of the web (although HTML may also be embedded by more savvy users).
- The generator software that formats the markup text and related asset files (e.g., images and stylesheets) into a static website usually consists of a single self-contained executable program, without dependencies on other components or plugins that may diverge over time, incurring potential maintenance issues.
- Static sites offer better performance by virtue of publishing browser-ready documents, without having to rely upon a content management system to build documents from database upon request.
- Static sites offer better security with a much smaller and less vulnerable attack surface available to be exploited by hackers and malware.
- A version control system (e.g., using <a target="_blank" rel="noopener noreferrer nofollow" href="https://git-scm.com/">Git</a>) may be used for content management, allowing for historical information to be restored in the event of some mistake or accidental damage (e.g., deleted information).
- A static site may be maintained as a version controlled repository on a free-tier service provider's platform allowing for easy deployment for publication via a continuous integration pipeline whenever a new version is uploaded.

However, this approach is not without its challenges. It requires users to be sufficiently technically savvy to install and use the necessary free/libre open source software. It's eseentially a trade-off between learning this approach or how to use one of many online website builder platforms.

This, my personal website, is an example of a static website built using a site generator and deployed via <a target="_blank" rel="noopener noreferrer nofollow" href="https://about.gitlab.com/stages-devops-lifecycle/pages/">Gitlab Pages</a>.
-->

### Contact

<a class="mailto" href="mailto:" target="_blank"><img src="/images/mailto.png" alt="Click to send email." title="Click to send email." /></a>

For more information or a quotation, please <a class="mailto" href="mailto: " target="_blank">email me</a>.

