This project contains the source for [mjbrown.github.io](https://mjbrown.github.io), built using the [Zola](https://getzola.org/) static site generator.

---

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

