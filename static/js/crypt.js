/*! Copyright 2021 Murray J Brown. */

(function(root) {
    "use strict";
    
    let emailAddress = "";
    
    function decryptEmailAddress() {
    	// Refer to https://github.com/ricmoo/aes-js
    	
		// 128-bit key (16 bytes)
		// let key = [ 0x0A, 0xBB, 0x89, 0x81, 0x47, 0xF8, 0x88, 0x24, 0x8A, 0x93, 0x84, 0x6C, 0x47, 0x79, 0x80, 0x2B ];
		let key = aesjs.utils.hex.toBytes("0ABB898147F888248A93846C4779802B");
		// The initialization vector (must be 16 bytes)
		let iv = aesjs.utils.hex.toBytes("6AC142DAB1B80279A013CB5FB646941F");	
		let encHex = "f1044c838f04a2733fab8aaece59280b"
		
		// Decrypt the hex string, convert it back to bytes
		let encBytes = aesjs.utils.hex.toBytes(encHex);
		// The cipher-block chaining mode of operation maintains internal
		// state, so to decrypt a new instance must be instantiated.
		let aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
		let decBytes = aesCbc.decrypt(encBytes);
		// Convert our bytes back into text
		let decText = aesjs.utils.utf8.fromBytes(decBytes);
		// console.log(decryptedText);
		
		return decText;
	}
	
	function getEmailAddress() {
	    if ( !emailAddress ) {
	        emailAddress = decryptEmailAddress();
	    }
	    
	    return emailAddress;
	}
	
	root.crypt = {
		getEmailAddress
	};
	
})(this);

